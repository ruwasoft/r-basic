
x <- c(151,174,138,186,128,136,179,163,152,131) #predictor vector

y <- c(63,81,56,91,47,57,76,72,62,48) #response vector



#find weight of a person with height 170---------------------

relation <- lm(y~x) #apply the lm() function

a <- data.frame(x=170)

result <- predict(relation,a)

result

#--------------------------------------------------------------



#Visualize the Regression Graphically---------------------------

png(file="linearRegression.png")

plot(
  y,
  x,
  col = "blue",
  main = "Height & Weight Regression",
  abline(lm(x~y)),
  cex = 1.3,
  pch = 16,
  xlab = "Weight in Kg",
  ylab = "Height in cm",
  )

dev.off()

#-------------------------------------------------------------
