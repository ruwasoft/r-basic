#install package
install.packages("party")

#load the party package. it will automatically load other dependent packages
library(party)

print(head(readingSkills))

#create a input data frame
input.dat <- readingSkills[c(1:105),]

#give a chart file name
png(file="decision_tree.png")

#create the tree
output.tree <- ctree(
  nativeSpeaker~age+shoeSize+score,
  data = input.dat
)

#plot the tree
plot(output.tree)

#save the file
dev.off()
