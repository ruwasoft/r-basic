head(mtcars)

#create a boxplot
png(filename = "boxplot.png")

boxplot(
  mtcars$mpg,
  main="mileage data boxplot",
  ylab="miles per gallon(mpg)",
  xlab="no. of cylinders",
  col="red",
  notch = FALSE
)

dev.off()



#create a boxpolt with formula
png(filename = "boxplotFormula.png")

boxplot(
  mpg ~ cyl,
  data =  mtcars,
  main="mileage data boxplot",
  ylab="miles per gallon(mpg)",
  xlab="no. of cylinders",
  col="red",
  notch = FALSE
)

dev.off()
