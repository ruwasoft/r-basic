#install and load package

install.packages("xlsx")

library("xlsx")

#------------------------------------------

#read xlsx file
data <- read.xlsx(
  "studentinfo.xlsx",
  sheetIndex = 1,
  )

data


#read xlsx file with Range of Rows
data <- read.xlsx(
  "studentinfo.xlsx",
  sheetIndex = 1,
  rowIndex = 1:5
)

data


#read xlsx file with Range of columns
data <- read.xlsx(
  "studentinfo.xlsx",
  sheetIndex = 1,
  colIndex = 1:3
)

data


#read xlsx file with startRow argument
data <- read.xlsx(
  "studentinfo.xlsx",
  sheetIndex = 1,
  startRow = 3
)

data


#-------------------------------------------

# create a data frame

dataframe1 <- data.frame (
  Name = c("Juan", "Alcaraz", "Simantha"),
  Age = c(22, 15, 19),
  Vote = c(TRUE, FALSE, TRUE))

#write into xlsx file

write.xlsx(dataframe1, "file1.xlsx")

#-------------------------------------------

# rename current worksheet
write.xlsx(dataframe1, "file1.xlsx",
           sheetName = "Voting Eligibility"
)
