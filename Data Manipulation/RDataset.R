airquality #New York Air Quality Measurements

AirPassengers #Monthly Airline Passenger Numbers 1949-1960

mtcars #Motor Trend Car Road Tests

iris #Edgar Anderson's Iris Data

#------------------------------------------------------------
#Get informations of datasets

cat("Dimension:",dim(airquality)) #use dim() to get dimension of dataset

cat("\nRow:",nrow(airquality)) #use nrow() to get number of rows

cat("\nColumn:",ncol(airquality)) #use ncol() to get number of columns

cat("\nName of Variables:",names(airquality)) #use names() to get name of variable of dataset

#-------------------------------------------------------------
#Display Variables Value in dataset

print(airquality$Temp) #display all values of Temp variable

#-------------------------------------------------------------
#Sort Variables Value in dataset

sort(airquality$Temp) #sort values of Temp variable

#-------------------------------------------------------------
#Statistical Summary of Data

summary(airquality$Temp) #get statistical summary of Temp variable

