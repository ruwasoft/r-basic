numbers <- c(2,4,6,8,10)

# return minimum value present in numbers
min(numbers)  # 2

characters <- c("s", "a", "p", "b")

# return alphabetically minimum value in characters
min(characters)  # "a"

# return largest value present in numbers
max(numbers)  # 10

# return alphabetically maximum value in characters
max(characters)  # "s"

#-------------------------------------------------------

#min() and max() in R with NA Values

numbers <- c(2, NA, 6, 7, NA, 10)

# return smallest value 
min(numbers)  # NA

# return smallest value 
min(numbers, na.rm = TRUE)  # 2

#--------------------------------------------------------

#min() and max() in a Data Frame

# Create a data frame
dataframe1 <- data.frame (
  Name = c("Juan", "Kay", "Jay", "Ray", "Aley"),
  Age = c(22, 15, 19, 30, 23),
  ID = c(101, 102, 103, 104, 105)
)

# return maximum value of Age column of dataframe1
print(max(dataframe1$Age)) # 30

# return minimum value of ID column of dataframe1 
print(min(dataframe1$ID)) # 101







