library(RMySQL)
library(mapdata)
library(ggplot2)
library(ggfortify)
library(sp)

dbcon = dbConnect(MySQL(), user='root', password='', dbname='track', host='localhost')
result = dbSendQuery(dbcon, "select lat,lon from data_location")
stations = fetch(result, n=-1)

File <- "D:/R Studio/r-basic/Maps/graph_four.png"
if (file.exists(File)) stop(File, " already exists")
dir.create(dirname(File), showWarnings = FALSE)


png(File,width=800, height=500)

jp <-  map('world2', 'Sri Lanka', plot = FALSE, fill = TRUE)
p <- autoplot(jp, geom = 'polygon', fill = 'subregion') + theme(legend.position="none")
df <- data.frame(long = stations$lon,lat = stations$lat)
coordinates(df) <- ~ long + lat
autoplot(df, p = p, colour = 'red', size = 4)
dev.off()