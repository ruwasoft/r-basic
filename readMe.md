# R basic

This project is created to get the basic knowlage about R language.

I followed a [tutorial from a web site.](https://www.programiz.com/r/getting-started)


![Bar Chart](https://gitlab.com/ruwasoft/r-basic/-/raw/main/Data%20Visualization/Bar%20Chart.png)

![Histogram](https://gitlab.com/ruwasoft/r-basic/-/raw/main/Data%20Visualization/Histogram.png)

![Pie Chart](https://gitlab.com/ruwasoft/r-basic/-/raw/main/Data%20Visualization/Pie%20Chart.png)

![3D Pie Chart](https://gitlab.com/ruwasoft/r-basic/-/raw/main/Data%20Visualization/3D%20Pie%20Chart.png)

![BoxPlot](https://gitlab.com/ruwasoft/r-basic/-/raw/main/Data%20Visualization/boxplot.png)

![Plot sequence of points](https://gitlab.com/ruwasoft/r-basic/-/raw/main/Data%20Visualization/PlotSequenceOfPoint.png)


